import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { ScrollContext } from "react-router-scroll-4";
import {IntlProvider } from "react-redux-multilingual";
import "./index.css";

//Layouts Export

import MainComponent from "./components/layouts/main";

//Import Custom Components
import store from "./store";
import Layout from "./components/app";
import testComponet from "./components/pages/testComponet";
import aboutComponet from "./components/pages/aboutComponet";
import ourServicesComponet from "./components/pages/ourServicesComponet";
import ourVehiclesComponet from "./components/pages/ourVehiclesComponet";
import packageComponet from "./components/pages/packageComponet";
import contactComponet from "./components/pages/contactComponet";
import rideWithCabgoComponet from "./components/pages/rideWithCabgoComponet";
import myDriverDashboardComponet from "./components/pages/myDriverDashboardComponet";
import signInComponet from "./components/pages/signInComponet";
import signUpComponet from "./components/pages/signUpComponet";
import partnerProfileComponet from "./components/pages/partnerProfileComponet";
import passengerDashboardComponet from "./components/pages/passengerDashboardComponet";

//Console Error
console.log = console.warn = console.error = () => {};

//Collection of pages
class Root extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            SomethingsDemo: []
        };
    }
    render(){
        return(
            <Provider store={store}>
                <IntlProvider>
                    <BrowserRouter basename={"/"}>
                        <ScrollContext>
                            <Switch>
                                <Route exact path={`${process.env.PUBLIC_URL}/`} component={MainComponent} />
                                <Route path={`${process.env.PUBLIC_URL}/ride-with-cabgo`} component={rideWithCabgoComponet} />
                                <Route path={`${process.env.PUBLIC_URL}/my-driver-dashboard`} component={myDriverDashboardComponet} />
                                <Route path={`${process.env.PUBLIC_URL}/sign-in`} component={signInComponet} />
                                <Route path={`${process.env.PUBLIC_URL}/sign-up`} component={signUpComponet} />
                                <Route path={`${process.env.PUBLIC_URL}/partner-profile`} component={partnerProfileComponet} />
                                <Route path={`${process.env.PUBLIC_URL}/my-passanger-dashboard`} component={passengerDashboardComponet} />

                                <Layout>
                                    <Route path={`${process.env.PUBLIC_URL}/about`} component={aboutComponet} />
                                    <Route path={`${process.env.PUBLIC_URL}/our-services`} component={ourServicesComponet} />
                                    <Route path={`${process.env.PUBLIC_URL}/our-vehicles`} component={ourVehiclesComponet} />
                                    <Route path={`${process.env.PUBLIC_URL}/packages`} component={packageComponet} />
                                    <Route path={`${process.env.PUBLIC_URL}/contact-us`} component={contactComponet} />
                                </Layout>
                            </Switch>
                        </ScrollContext>
                    </BrowserRouter>
                </IntlProvider>
            </Provider>
        );
    }
}

ReactDOM.render(<Root/>, document.getElementById('root'));

