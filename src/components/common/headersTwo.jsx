import React, { Component } from "react";
import { Link } from "react-router-dom";
import { IntlActions } from "react-redux-multilingual";
import Pace from "react-pace-progress";

// Import custom components
import { connect } from "react-redux";

class HeaderTwo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isActive:''
        };
    }

    componentDidMount() {

    }
    //Set data
    componentWillMount() {

    }
    //Un set data
    componentWillUnmount() {

    }

    render() {
        return (
            <div>
                <header className="theme-3">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <div className="logo">
                                    <Link to={`${process.env.PUBLIC_URL}/`}><img src={`${process.env.PUBLIC_URL}/assets/images/logo-main.png`}  alt="" /></Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            </div>
        );
    }
}

//If Need Any State Value Used For redux state
const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(HeaderTwo);
