import React, { Component } from "react";
import { Link } from "react-router-dom";
import { IntlActions } from "react-redux-multilingual";
import Pace from "react-pace-progress";

// Import custom components
import { connect } from "react-redux";

class Headers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isActive:''
        };
    }

    componentDidMount() {

    }
    //Set data
    componentWillMount() {

    }
    //Un set data
    componentWillUnmount() {

    }

    render() {
        var currentRoutes = window.location.pathname;
        return (
            <div>
                <header className="theme-1">
                    <section className="header__upper">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="header__upper--left">
                                        <div className="logo">
                                            <Link to={`${process.env.PUBLIC_URL}/`}><img src={`${process.env.PUBLIC_URL}/assets/images/logo-main.png`}  alt="" /></Link>
                                        </div>
                                        <div className="search-bar">
                                            <form action className="form">
                                                <span className="icon icon-left"><i className="fas fa-map-marker-alt" /></span>
                                                <input className="form-control" type="search" name placeholder="Tell us your location" id />
                                                <button className="button button-dark" type="submit"><img src={`${process.env.PUBLIC_URL}/assets/images/arrow-shape.png`} alt="" /></button>
                                            </form>
                                            <span className="mobile-search-icon d-block d-md-none"><i className="fas fa-search" /></span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="header__upper--right">
                                        <nav className="navigation">
                                            <ul>
                                                <li>
                                                    <Link to={`${process.env.PUBLIC_URL}/ride-with-cabgo`}>Ride</Link>
                                                </li>
                                                <li>
                                                    <Link to={`${process.env.PUBLIC_URL}/my-driver-dashboard`}>Drive</Link>
                                                </li>

                                                <li>
                                                    <Link to={`${process.env.PUBLIC_URL}/contact-us`}>Drive</Link>
                                                </li>
                                                <li>
                                                    <Link to={`${process.env.PUBLIC_URL}/contact-us`}>Sign in</Link>
                                                </li>
                                            </ul>
                                        </nav>

                                        <Link to={`${process.env.PUBLIC_URL}/ride-with-cabgo`} className="button button-dark big">Ride with cabgo</Link>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="header__lower">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-12">
                                    <nav className="navbar navbar-expand-lg navbar-light">
                                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                            <span className="navbar-toggler-icon" />
                                        </button>
                                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                            <ul className="navbar-nav mr-auto">
                                                <li className={(currentRoutes == '/' ? 'active nav-item' : 'nav-item')}>
                                                    <Link to={`${process.env.PUBLIC_URL}/`}  className="nav-link" onClick={() => this.setState({isActive: 'home'})}><i className="fas fa-home" />Home</Link>
                                                </li>
                                                <li className={(currentRoutes == '/about' ? 'active nav-item' : 'nav-item')}>
                                                    <Link to={`${process.env.PUBLIC_URL}/about`}  className="nav-link"  onClick={() => this.setState({isActive: 'about'})} ><i className="fas fa-exclamation-circle" />About</Link>
                                                </li>
                                                <li className={(currentRoutes == '/our-services' ? 'active nav-item' : 'nav-item')}>
                                                    <Link to={`${process.env.PUBLIC_URL}/our-services`}  className="nav-link" ><i className="fas fa-taxi" />Our Services</Link>
                                                </li>
                                                <li className={(currentRoutes == '/our-vehicles' ? 'active nav-item' : 'nav-item')}>
                                                    <Link to={`${process.env.PUBLIC_URL}/our-vehicles`}  className="nav-link"><i className="fas fa-taxi" />Our Vehicles</Link>
                                                </li>
                                                <li className={(currentRoutes == '/packages' ? 'active nav-item' : 'nav-item')}>
                                                    <Link to={`${process.env.PUBLIC_URL}/packages`}  className="nav-link"><i className="fas fa-cube" />Packages</Link>
                                                </li>
                                                <li className={(currentRoutes == '/contact-us' ? 'active nav-item' : 'nav-item')}>
                                                    <Link to={`${process.env.PUBLIC_URL}/contact-us`}  className="nav-link"><i className="fas fa-map-marker-alt" />Contacts</Link>
                                                </li>
                                            </ul>
                                            <div className="my-2 my-lg-0">
                                                <Link to={`${process.env.PUBLIC_URL}/sign-up`}   className="button button-light big">Become a Driver</Link>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </section>
                </header>
            </div>
        );
    }
}

//If Need Any State Value Used For redux state
const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(Headers);
