import React, { Component } from "react";
import { Link } from "react-router-dom";
import { IntlActions } from "react-redux-multilingual";
import Pace from "react-pace-progress";

// Import custom components
import { connect } from "react-redux";

class FootersOne extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {

    }
    //Set data
    componentWillMount() {

    }
    //Un set data
    componentWillUnmount() {

    }
    render() {
        return (
            <div>
                <footer className="theme-2">
                    <section className="footer-nav-section section-padding theme-2">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-3">
                                    <div className="footer-brand">
                                        <a href="/"><img src={`${process.env.PUBLIC_URL}/assets/images/logo.png`}  alt="logo" /></a>
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <ul className="social-nav">
                                        <li><a href="#" className="facebook"><i className="fab fa-facebook-f" /></a></li>
                                        <li><a href="#" className="twitter"><i className="fab fa-twitter" /></a></li>
                                        <li><a href="#" className="instagram"><i className="fab fa-instagram" /></a></li>
                                        <li><a href="#" className="google-p"><i className="fab fa-google-plus-g" /></a></li>
                                        <li><a href="#" className="linkedin"><i className="fab fa-linkedin-in" /></a></li>
                                        <li><a href="#" className="pinterest"><i className="fab fa-pinterest-p" /></a></li>
                                    </ul>
                                </div>
                                <div className="col-lg-3">
                                    <div className="app-download-box">
                                        <a href="#"><img src={`${process.env.PUBLIC_URL}/assets/images/icon/google-play.jpg`} alt="Google play" /></a>
                                        <a href="#"><img src={`${process.env.PUBLIC_URL}/assets/images/icon/apple-store.jpg`} alt="Apple store" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="copyright-section theme-2">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6">
                                    <p>© Copyright 2018 by Tortoiz. All Right Reserved.</p>
                                </div>
                                <div className="col-lg-6">
                                    <ul className="social-nav">
                                        <li><a href="#">Privecy</a></li>
                                        <li><a href="#">Terms</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </footer>
            </div>
        );
    }
}

//If Need Any State Value Used For redux state
const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(FootersOne);
