import React, { Component } from "react";
import { Link } from "react-router-dom";
import { IntlActions } from "react-redux-multilingual";
import Pace from "react-pace-progress";

// Import custom components
import { connect } from "react-redux";

class HeaderOne extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isActive:''
        };
    }

    componentDidMount() {

    }
    //Set data
    componentWillMount() {

    }
    //Un set data
    componentWillUnmount() {

    }

    render() {
        var currentRoutes = window.location.pathname;
        return (
            <div>
                <header className="theme-2">
                    <section className="header__upper">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6">
                                    <div className="header__upper--left">
                                        <div className="logo">
                                            <Link to={`${process.env.PUBLIC_URL}/`}><img src={`${process.env.PUBLIC_URL}/assets/images/logo-main.png`}  alt="" /></Link>
                                        </div>
                                        <div className="search-bar">
                                            <form action className="form">
                                                <span className="icon icon-left"><i className="fas fa-map-marker-alt" /></span>
                                                <input className="form-control" type="search" name placeholder="Tell us your location" id />
                                                <button className="button button-dark" type="submit"><i className="fas fa-arrow-right" /></button>
                                            </form>
                                            <span className="mobile-search-icon d-block d-md-none"><i className="fas fa-search" /></span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6">
                                    <div className="header__upper--right">
                                        <nav className="navigation">
                                            <ul>
                                                <li>
                                                    <Link to={`${process.env.PUBLIC_URL}/ride-with-cabgo`}>Ride</Link>
                                                </li>
                                                <li>
                                                    <Link to={`${process.env.PUBLIC_URL}/my-driver-dashboard`}>Drive</Link>
                                                </li>

                                                <li>
                                                    <Link to={`${process.env.PUBLIC_URL}/contact-us`}>Help</Link>
                                                </li>
                                                <li>
                                                    <Link to={`${process.env.PUBLIC_URL}/sign-in`}>Sign in</Link>
                                                </li>

                                                <li>
                                                    <Link to={`${process.env.PUBLIC_URL}/contact-us`}><i className="far fa-envelope" /></Link>
                                                    </li>
                                            </ul>
                                        </nav>
                                        <div className="dropdown">
                                            <a href="#" className="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <div className="media">
                                                    <img height={30} width={30} className="mr-3" src={`${process.env.PUBLIC_URL}/assets/images/partner-img.png`} alt="" />
                                                    <div className="media-body">
                                                        <h6 className="m-0">John Doe <i className="fas fa-angle-down" /></h6>
                                                        <p className="m-0">India</p>
                                                    </div>
                                                </div>
                                            </a>
                                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                <Link to={`${process.env.PUBLIC_URL}/partner-profile`} className="dropdown-item">Partner Profile</Link>
                                                <Link to={`${process.env.PUBLIC_URL}/my-passanger-dashboard`} className="dropdown-item">My passanger dashboard</Link>
                                                <a className="dropdown-item" href="#">Another action</a>
                                                <a className="dropdown-item" href="#">Something else here</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </header>
            </div>
        );
    }
}

//If Need Any State Value Used For redux state
const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(HeaderOne);
