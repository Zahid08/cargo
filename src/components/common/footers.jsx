import React, { Component } from "react";
import { Link } from "react-router-dom";
import { IntlActions } from "react-redux-multilingual";
import Pace from "react-pace-progress";

// Import custom components
import { connect } from "react-redux";

class Footers extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {

    }
    //Set data
    componentWillMount() {

    }
    //Un set data
    componentWillUnmount() {

    }
    render() {
        return (
            <div>
                <footer className="footer-section theme-1">
                    <section className="footer-nav-section section-padding theme-1">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-3 col-sm-6">
                                    <div className="footer-brand">
                                        <Link to={`${process.env.PUBLIC_URL}/`}><img src={`${process.env.PUBLIC_URL}/assets/images/logo.png`} alt="Logo" /></Link>
                                    </div>
                                    <div className="footer-text">
                                        <p>Nulla justo neque, tincidunt id bibendum a, rhoncus et eros. Vestibulum commodo diam ut risus pulvinar consequat vitae a dui. Vivamus sed molestie diam. Maecenas vitae enim lacus.</p>
                                    </div>
                                    <div className="helpline">
                                        <h4>Toll Free <span>Helpline</span></h4>
                                        <p>(+1) 123 4567 890</p>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-sm-6">
                                    <h4>Useful links</h4>
                                    <nav className="footer-navigation">
                                        <ul>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/`} >Home</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/about`} >About</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/our-vehicles`} >Our vehicles</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/our-services`} >Services</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/packages`} >Packages</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/sign-in`} >Login</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/sign-up`} >Register</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/blog`} >Blog</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/testmonial`} >Testimonials</Link>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/ride-with-cabgo`} >Ride</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/my-driver-dashboard`} >Drive</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/sign-up`} >Become a Driver</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/sign-up`} >Terms &amp; Conditions</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/press`} >Press</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/help`} >Help</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/policy`} >Privecy policy</Link>
                                            </li>
                                            <li>
                                                <Link to={`${process.env.PUBLIC_URL}/cookies-policy`} >Cookies policy</Link>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div className="col-lg-3 col-sm-6">
                                    <h4>Head Office</h4>
                                    <address className="company-address">
                                        <p className="m-b-20">15 Street No, Ox Building, Near Station, 1356. </p>
                                        <p className="m-b-8">Phone Numver : (+1) 123 4567 890</p>
                                        <p className="m-b-8">Email Address : Cabgo@gmail.com</p>
                                        <p className="m-b-8">Fax : Service@cabgo.com</p>
                                    </address>
                                </div>
                                <div className="col-lg-3 col-sm-6">
                                    <h4>Download Mobile App</h4>
                                    <div className="app-download-box">
                                        <a href="#"><img src={`${process.env.PUBLIC_URL}/assets/images/icon/google-play.jpg`} alt="Google play" /></a>
                                        <a href="#"><img src={`${process.env.PUBLIC_URL}/assets/images/icon/apple-store.jpg`}  alt="Apple store" /></a>
                                    </div>
                                    <div className="cta-button">
                                        <Link to={`${process.env.PUBLIC_URL}/my-driver-dashboard`}  className="button button-dark">Become a Driver</Link>
                                        <Link to={`${process.env.PUBLIC_URL}/ride-with-cabgo`}  className="button button-dark">Ride with CabGo</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="copyright-section">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-6">
                                    <p>© Copyright 2018 by Tortoiz. All Right Reserved.</p>
                                </div>
                                <div className="col-lg-6">
                                    <ul className="social-nav">
                                        <li><a href="#" className="facebook"><i className="fab fa-facebook-f" /></a></li>
                                        <li><a href="#" className="twitter"><i className="fab fa-twitter" /></a></li>
                                        <li><a href="#" className="instagram"><i className="fab fa-instagram" /></a></li>
                                        <li><a href="#" className="google-p"><i className="fab fa-google-plus-g" /></a></li>
                                        <li><a href="#" className="linkedin"><i className="fab fa-linkedin-in" /></a></li>
                                        <li><a href="#" className="pinterest"><i className="fab fa-pinterest-p" /></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    {/*<section className="map-section">*/}
                        {/*<div className="google-map">*/}
                            {/*<div id="map-canvas" />*/}
                        {/*</div>*/}
                    {/*</section>*/}
                </footer>
            </div>
        );
    }
}

//If Need Any State Value Used For redux state
const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(Footers);
