import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

// Import custom components
import _ from "lodash";
import Headers from "../common/headers";
import Footers from "../common/footers";
import HomeComponets from "../pages/homeComponet";
import SlidersComponet from "../pages/slidersComponet";

class MainComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }
  render() {
    let basciUrl=`${process.env.PUBLIC_URL}`;
    console.log(basciUrl);
    
    return (
        <div>
          <Headers/>
            <div>
                <SlidersComponet />
                <HomeComponets />
            </div>
          <Footers/>
        </div>
    );
  }
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(MainComponent);
