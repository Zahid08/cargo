import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import HeaderOne from "../common/headersOne";
import FootersOne from "../common/footersOne";


class RideWithCabgoComponet extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }
  render() {
      console.log(`${process.env.REACT_APP_API_URL}`);
    return (
        <div>
            <HeaderOne/>
       <div>
           <section className="breadcrumb-section">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6">
                           <ol className="breadcrumb">
                               <li><a href={`${process.env.PUBLIC_URL}/ride-with-cabgo`}>Ride with Cabgo</a></li>
                           </ol>
                       </div>
                   </div>
               </div>
           </section>
           <section className="section-padding our-vehicles-section">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6">
                           <div className="booking-form">
                               <form action="#">
                                   <div className="from-group destination">
                                       <label htmlFor>From</label>
                                       <i className="fas fa-map-marker-alt" />
                                       <input type="text" name placeholder="Select Desgination" id className="form-control" />
                                   </div>
                                   <div className="from-group destination">
                                       <label htmlFor>Where to?</label>
                                       <i className="fas fa-map-marker-alt" />
                                       <input type="text" name placeholder="Select Desgination" id className="form-control" />
                                   </div>
                                   <div className="payment-options-wrapper">
                                       <label htmlFor>Payment Method</label>
                                       <div className="from-group payment-options">
                                           <div className="form-check form-check-inline">
                                               <input className="form-check-input" type="radio" name="payment-opts" id="cash-pay" defaultValue="option1" />
                                               <label className="form-check-label" htmlFor="cash-pay">Cash</label>
                                           </div>
                                           <div className="form-check form-check-inline">
                                               <input className="form-check-input" type="radio" name="payment-opts" id="banking-pay" defaultValue="option2" />
                                               <label className="form-check-label" htmlFor="banking-pay">Net Banking</label>
                                           </div>
                                           <div className="form-check form-check-inline">
                                               <input className="form-check-input" type="radio" name="payment-opts" id="card-pay" defaultValue="option3" />
                                               <label className="form-check-label" htmlFor="card-pay">Debit Card</label>
                                           </div>
                                       </div>
                                   </div>
                                   <div className="select-car-wrapper">
                                       <h2>Selected Car</h2>
                                       <div className="selected-car">
                                           <div className="from-group car-options">
                                               <div className="form-check form-check-inline">
                                                   <input className="form-check-input" type="radio" name="car-opts" id="scooter" defaultValue="option1" />
                                                   <label className="form-check-label" htmlFor="scooter">
                                                       <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/car-1.png`}  alt="car" />
                                                   </label>
                                                   <div className="car-details">
                                                       <h4>1x</h4>
                                                       <p>Scooter</p>
                                                   </div>
                                               </div>
                                               <div className="form-check form-check-inline">
                                                   <input className="form-check-input" type="radio" name="car-opts" id="alto" defaultValue="option2" />
                                                   <label className="form-check-label" htmlFor="alto">
                                                       <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/car-2.png`} alt="Car" />
                                                   </label>
                                                   <div className="car-details">
                                                       <h4>2x</h4>
                                                       <p>Alto</p>
                                                   </div>
                                               </div>
                                               <div className="form-check form-check-inline">
                                                   <input className="form-check-input" type="radio" name="car-opts" id="swift" defaultValue="option3" />
                                                   <label className="form-check-label" htmlFor="swift">
                                                       <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/car-3.png`}  alt="Car" />
                                                   </label>
                                                   <div className="car-details">
                                                       <h4>3x</h4>
                                                       <p>Swift dzire</p>
                                                   </div>
                                               </div>
                                               <div className="form-check form-check-inline">
                                                   <input className="form-check-input" type="radio" name="car-opts" id="luxury" defaultValue="option3" />
                                                   <label className="form-check-label" htmlFor="luxury">
                                                       <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/car-4.png`} alt="Car" />
                                                   </label>
                                                   <div className="car-details">
                                                       <h4>4x</h4>
                                                       <p>Luxury</p>
                                                   </div>
                                               </div>
                                               <div className="form-check form-check-inline">
                                                   <input className="form-check-input" type="radio" name="car-opts" id="tourist" defaultValue="option3" />
                                                   <label className="form-check-label" htmlFor="tourist">
                                                       <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/car-5.png`}  alt="Car" />
                                                   </label>
                                                   <div className="car-details">
                                                       <h4>5x</h4>
                                                       <p>Tourist</p>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <button type="submit" className="button button-dark tiny">Book Now</button>
                               </form>
                           </div>
                       </div>
                       <div className="col-lg-6">
                           <div className="ride-map-area">
                               {/*<div id="ride-map" />*/}
                               <iframe frameBorder="0" style={{ width: "100%", height: "69vh"}}
                                       src="https://www.google.com/maps/embed/v1/place?q=40.7127837,-74.0059413&amp;key=AIzaSyCc3zoz5TZaG3w2oF7IeR-fhxNXi8uywNk">
                               </iframe>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       </div>
            <FootersOne/>
        </div>
    );
  }
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(RideWithCabgoComponet);
