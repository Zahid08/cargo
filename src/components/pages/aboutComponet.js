import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import OwlCarousel from "react-owl-carousel";


class AboutComponets extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }

  render() {
    return (
       <div>
           {/*------BreadCamp---------------*/}
           <section className="breadcrumb-section">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6">
                           <ol className="breadcrumb">
                               <li>
                                   <Link to={`#`}>Ride with Cabgo</Link>
                               </li>
                           </ol>
                       </div>
                       <div className="col-lg-6">
                           <div className="navigation-lower">
                               <ul>
                                   <li>
                                       <Link to={`${process.env.PUBLIC_URL}/about`} >About</Link>
                                   </li>
                                   <li>
                                       <Link to={`${process.env.PUBLIC_URL}/blog`} >Blog</Link>
                                   </li>
                                   <li>
                                       <Link to={`${process.env.PUBLIC_URL}/career`} >Career</Link>
                                   </li>
                                   <li>
                                       <Link to={`${process.env.PUBLIC_URL}/media-post`} >Media Posts</Link>
                                   </li>
                                   <li>
                                       <Link to={`${process.env.PUBLIC_URL}/help`} >Career</Link>
                                   </li>
                               </ul>
                           </div>
                       </div>
                   </div>
               </div>
           </section>

           <section className="about-us-area section-padding">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6">
                           <h2 className="section-title">About us</h2>
                           <div className="about-us-text">
                               <h4>Trusted Cab Services in All World</h4>
                               <p>Curabitur placerat cursus nisi nec pharetra. Proin quis tortor fringilla, placerat nisi nec, auctor ex. Donec commodo orci ac lectus mattis, sed interdum sem scelerisque. Vivamus at euismod magna. Aenean semper risus nec dolor bibendum cursus. Donec eu odio eu ligula sagittis fringilla. Phasellus vulputate velit eu vehicula auctor. Nam vel pellentesque libero. Duis eget efficitur dui. Mauris tempor ex non tortor aliquet, et interdum mi dapibus. Phasellus ac dui nunc. Sed quis sagittis lorem, in blandit nibh. Fusce dui metus, interdum ac malesuada eu, ornare nec neque. Fusce hendrerit, tortor id egestas rutrum, orci lorem lacinia velit, sed mollis augue diam eget ipsum. Curabitur euismod, tellus sit amet tincidunt semper, dui odio pharetra orci, sed molestie odio libero sed libero. Sed volutpat ornare mauris. Sed gravida pulvinar urna, eget euismod mi mattis a. Vivamus sagittis eu quam sed ullamcorper. Etiam aliquet ornare tempus. Maecenas dictum nunc ac tortor rutrum, quis sollicitudin libero feugiat. Mauris iaculis sed risus ut tempus.</p>
                               <Link to={`#`} className="button button-dark tiny">Read More</Link>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <section className="section-padding counter-area">
               <div className="container">
                   <div className="row text-center">
                       <div className="col-md-3 col-6">
                           <div className="single-counter">
                               <div className="counter-icon">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/icon/counter-icon-1.png`} alt="" />
                               </div>
                               <h2>
                                   <span className="counter">7,50,000</span>
                               </h2>
                               <p className="counter-text">Vehicles</p>
                           </div>
                       </div>
                       <div className="col-md-3 col-6">
                           <div className="single-counter">
                               <div className="counter-icon">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/icon/counter-icon-2.png`} alt="" />
                               </div>
                               <h2>
                                   <span className="counter">220</span> +
                               </h2>
                               <p className="counter-text">Cities</p>
                           </div>
                       </div>
                       <div className="col-md-3 col-6">
                           <div className="single-counter">
                               <div className="counter-icon">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/icon/counter-icon-3.png`}  alt="" />
                               </div>
                               <h2>
                                   <span className="counter">5,50,000</span>
                               </h2>
                               <p className="counter-text">Entreprenurs Partners</p>
                           </div>
                       </div>
                       <div className="col-md-3 col-6">
                           <div className="single-counter">
                               <div className="counter-icon">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/icon/counter-icon4.png`} alt="" />
                               </div>
                               <h2>
                                   <span className="counter">75,00,000</span>
                               </h2>
                               <p className="counter-text">Passengers</p>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <section className="section-padding how-work-area">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6 offset-lg-3">
                           <h2 className="section-title text-center">How It Work</h2>
                       </div>
                   </div>

                   <div className="row">
                       <div className="col-lg-12 d-none d-lg-block">
                           <div className="icons-section">
                               <div className="single-icon">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/icon/1.png`}  alt="" />
                               </div>
                               <div className="single-icon">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/icon/2.png`}  alt="" />
                               </div>
                               <div className="single-icon">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/icon/3.png`} alt="" />
                               </div>
                               <div className="single-icon">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/icon/4.png`} alt="" />
                               </div>
                           </div>
                       </div>
                   </div>

                   <div className="row">
                       <div className="col-lg-3 col-sm-6">
                           <div className="single-icon text-center m-b-10 d-block d-lg-none">
                               <img src={`${process.env.PUBLIC_URL}/assets/images/icon/1.png`} alt="" />
                           </div>
                           <div className="how-work-text">
                               <h4>Book in Just 2 Tabs</h4>
                               <p>Curabitur ac quam aliquam urna vehicula semper sed vel elit. Sed et leo purus. Vivamus vitae sapien.</p>
                           </div>
                       </div>
                       <div className="col-lg-3 col-sm-6">
                           <div className="single-icon text-center m-b-10 d-block d-lg-none">
                               <img src={`${process.env.PUBLIC_URL}/assets/images/icon/2.png`} alt="" />
                           </div>
                           <div className="how-work-text">
                               <h4>Get a Driver</h4>
                               <p>Curabitur ac quam aliquam urna vehicula semper sed vel elit. Sed et leo purus. Vivamus vitae sapien.</p>
                           </div>
                       </div>
                       <div className="col-lg-3 col-sm-6">
                           <div className="single-icon text-center m-b-10 d-block d-lg-none">
                               <img src={`${process.env.PUBLIC_URL}/assets/images/icon/3.png`} alt="" />
                           </div>
                           <div className="how-work-text">
                               <h4>Track your Driver</h4>
                               <p>Curabitur ac quam aliquam urna vehicula semper sed vel elit. Sed et leo purus. Vivamus vitae sapien.</p>
                           </div>
                       </div>
                       <div className="col-lg-3 col-sm-6">
                           <div className="single-icon text-center m-b-10 d-block d-lg-none">
                               <img src={`${process.env.PUBLIC_URL}/assets/images/icon/4.png`} alt="" />
                           </div>
                           <div className="how-work-text">
                               <h4>Arrive safely</h4>
                               <p>Curabitur ac quam aliquam urna vehicula semper sed vel elit. Sed et leo purus. Vivamus vitae sapien.</p>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <section className="section-padding testimonial-area">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6 offset-lg-3">
                           <h2 className="section-title text-center">What about passanger says</h2>
                       </div>
                   </div>
                   <div className="row">
                       <div className="col-lg-12">
                           <OwlCarousel className="testimonial-carousel owl-carousel" loop={true} nav={true} autoWidth={false} items={1} center={false} id="testimonial-carousel-2" autoplay={true}>
                               <div className="single-testimonial-item text-center">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/client-1.png`}  alt="" className="client-img" />
                                   <p className="testimonial-text">Quisque venenatis sit amet libero vel laoreet. Maecenas et eros a metus vestibulum rhoncus. Aenean varius tincidunt libero at egestas. Aliquam eget interdum enim. Nulla malesuada dolor at turpis blandit sagittis. </p>
                                   <h4 className="client-name">John Doe</h4>
                                   <p className="theme-color">Passanger</p>
                               </div>
                               <div className="single-testimonial-item text-center">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/client-1.png`}  alt="" className="client-img" />
                                   <p className="testimonial-text">Quisque venenatis sit amet libero vel laoreet. Maecenas et eros a metus vestibulum rhoncus. Aenean varius tincidunt libero at egestas. Aliquam eget interdum enim. Nulla malesuada dolor at turpis blandit sagittis. </p>
                                   <h4 className="client-name">John Doe</h4>
                                   <p className="theme-color">Passanger</p>
                               </div>
                               <div className="single-testimonial-item text-center">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/client-1.png`}  alt="" className="client-img" />
                                   <p className="testimonial-text">Quisque venenatis sit amet libero vel laoreet. Maecenas et eros a metus vestibulum rhoncus. Aenean varius tincidunt libero at egestas. Aliquam eget interdum enim. Nulla malesuada dolor at turpis blandit sagittis. </p>
                                   <h4 className="client-name">John Doe</h4>
                                   <p className="theme-color">Passanger</p>
                               </div>
                           </OwlCarousel>;
                       </div>
                   </div>
               </div>
           </section>
           <section className="section-padding team-section">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6 offset-lg-3">
                           <h2 className="section-title text-center">The Duo Behind Cabgo</h2>
                       </div>
                   </div>
                   <div className="row">
                       <div className="col-lg-4 col-sm-6">
                           <div className="single-team-member">
                               <div className="member-img">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/driver-1.jpg`} alt="" />
                               </div>
                               <div className="member-info">
                                   <h4 className="member-name">John Doe</h4>
                                   <p className="theme-color">Co-Founder and CEO</p>
                                   <p>Duis leo ipsum, consequat sollicitudin enim id, accumsan condimentum felis. Donec nisl dolor, malesuada tincidunt ultricies et.</p>
                               </div>
                           </div>
                       </div>
                       <div className="col-lg-4 col-sm-6">
                           <div className="single-team-member">
                               <div className="member-img">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/driver-2.jpg`} alt="" />
                               </div>
                               <div className="member-info">
                                   <h4 className="member-name">John Doe</h4>
                                   <p className="theme-color">Co-Founder and CEO</p>
                                   <p>Duis leo ipsum, consequat sollicitudin enim id, accumsan condimentum felis. Donec nisl dolor, malesuada tincidunt ultricies et.</p>
                               </div>
                           </div>
                       </div>
                       <div className="col-lg-4 offset-lg-0 col-sm-6 offset-sm-3">
                           <div className="single-team-member">
                               <div className="member-img">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/driver-3.jpg`} alt="" />
                               </div>
                               <div className="member-info">
                                   <h4 className="member-name">John Doe</h4>
                                   <p className="theme-color">Co-Founder and CEO</p>
                                   <p>Duis leo ipsum, consequat sollicitudin enim id, accumsan condimentum felis. Donec nisl dolor, malesuada tincidunt ultricies et.</p>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       </div>
    );
  }
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(AboutComponets);
