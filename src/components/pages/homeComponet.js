import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import OwlCarousel from 'react-owl-carousel';


class HomeComponets extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }
  render() {
    return (
       <div>
           <section className="section-padding how-work-area">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6 offset-lg-3">
                           <h2 className="section-title text-center">How It Work</h2>
                       </div>
                   </div>
                   <div className="row">
                       <div className="col-lg-12 d-none d-lg-block">
                           <div className="icons-section">
                               <div className="single-icon">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/icon/1.png`} alt="" />
                               </div>
                               <div className="single-icon">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/icon/2.png`} alt="" />
                               </div>
                               <div className="single-icon">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/icon/3.png`} alt="" />
                               </div>
                               <div className="single-icon">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/icon/4.png`} alt="" />
                               </div>
                           </div>
                       </div>
                   </div>
                   <div className="row">
                       <div className="col-lg-3 col-sm-6">
                           <div className="single-icon text-center m-b-10 d-block d-lg-none">
                               <img src={`${process.env.PUBLIC_URL}/assets/images/icon/1.png`} alt="" />
                           </div>
                           <div className="how-work-text">
                               <h4>Book in Just 2 Tabs</h4>
                               <p>Curabitur ac quam aliquam urna vehicula semper sed vel elit. Sed et leo purus. Vivamus vitae sapien.</p>
                           </div>
                       </div>
                       <div className="col-lg-3 col-sm-6">
                           <div className="single-icon text-center m-b-10 d-block d-lg-none">
                               <img src={`${process.env.PUBLIC_URL}/assets/images/icon/2.png`} alt="" />
                           </div>
                           <div className="how-work-text">
                               <h4>Get a Driver</h4>
                               <p>Curabitur ac quam aliquam urna vehicula semper sed vel elit. Sed et leo purus. Vivamus vitae sapien.</p>
                           </div>
                       </div>
                       <div className="col-lg-3 col-sm-6">
                           <div className="single-icon text-center m-b-10 d-block d-lg-none">
                               <img src={`${process.env.PUBLIC_URL}/assets/images/icon/3.png`} alt="" />
                           </div>
                           <div className="how-work-text">
                               <h4>Track your Driver</h4>
                               <p>Curabitur ac quam aliquam urna vehicula semper sed vel elit. Sed et leo purus. Vivamus vitae sapien.</p>
                           </div>
                       </div>
                       <div className="col-lg-3 col-sm-6">
                           <div className="single-icon text-center m-b-10 d-block d-lg-none">
                               <img src={`${process.env.PUBLIC_URL}/assets/images/icon/4.png`} alt="" />
                           </div>
                           <div className="how-work-text">
                               <h4>Arrive safely</h4>
                               <p>Curabitur ac quam aliquam urna vehicula semper sed vel elit. Sed et leo purus. Vivamus vitae sapien.</p>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <section className="about-us-area bg-2 section-padding">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6">
                           <h2 className="section-title">About us</h2>
                           <div className="about-us-text">
                               <h4>Trusted Cab Services in All World</h4>
                               <p>Curabitur placerat cursus nisi nec pharetra. Proin quis tortor fringilla, placerat nisi nec, auctor ex. Donec commodo orci ac lectus mattis, sed interdum sem scelerisque. Vivamus at euismod magna. Aenean semper risus nec dolor bibendum cursus. Donec eu odio eu ligula sagittis fringilla. Phasellus vulputate velit eu vehicula auctor. Nam vel pellentesque libero. Duis eget efficitur dui. Mauris tempor ex non tortor aliquet, et interdum mi dapibus. Phasellus ac dui nunc. Sed quis sagittis lorem, in blandit nibh.</p>
                               <a href="#" className="button button-dark tiny">Read More</a>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <section className="download-section section-padding p-b-0">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6 offset-lg-3">
                           <h2 className="section-title text-center">Download CabGo</h2>
                       </div>
                   </div>
                   <div className="row">
                       <div className="col-lg-5">
                           <div className="download-qrcode">
                               <img src={`${process.env.PUBLIC_URL}/assets/images/qr.png`}  alt="" />
                           </div>
                       </div>
                       <div className="col-lg-7">
                           <div className="download-text">
                               <h2>Download the cabgo mobile application</h2>
                               <p>Nunc volutpat tincidunt est a scelerisque. Aliquam erat volutpat. Donec varius ex in justo pharetra, nec mollis erat porta. Donec sit amet facilisis neque. In hac habitasse platea dictumst.</p>
                           </div>
                           <div className="download-buttons">
                                    <a href="#">
                                        <img src={`${process.env.PUBLIC_URL}/assets/images/download-1.png`}  alt="" />
                                   </a>
                               <a href="#"><img src={`${process.env.PUBLIC_URL}/assets/images/download-2.png`} alt="" /></a>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <section className="section-padding testimonial-area">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6 offset-lg-3">
                           <h2 className="section-title text-center">What about passanger says</h2>
                       </div>
                   </div>
                   <div className="row">
                       <div className="col-lg-12">
                               <OwlCarousel className="testimonial-carousel owl-carousel" loop={true} nav={true} autoWidth={false} items={1} center={false} id="testimonial-carousel-1" autoplay={true}>
                               <div className="single-testimonial-item text-center">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/client-1.png`} alt="" className="client-img" />
                                   <p className="testimonial-text">Quisque venenatis sit amet libero vel laoreet. Maecenas et eros a metus vestibulum rhoncus. Aenean varius tincidunt libero at egestas. Aliquam eget interdum enim. Nulla malesuada dolor at turpis blandit sagittis. </p>
                                   <h4 className="client-name">John Doe</h4>
                                   <p className="theme-color">Passanger</p>
                               </div>
                               <div className="single-testimonial-item text-center">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/client-1.png`}  alt="" className="client-img" />
                                   <p className="testimonial-text">Quisque venenatis sit amet libero vel laoreet. Maecenas et eros a metus vestibulum rhoncus. Aenean varius tincidunt libero at egestas. Aliquam eget interdum enim. Nulla malesuada dolor at turpis blandit sagittis. </p>
                                   <h4 className="client-name">John Doe</h4>
                                   <p className="theme-color">Passanger</p>
                               </div>
                               <div className="single-testimonial-item text-center">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/client-1.png`} alt="" className="client-img" />
                                   <p className="testimonial-text">Quisque venenatis sit amet libero vel laoreet. Maecenas et eros a metus vestibulum rhoncus. Aenean varius tincidunt libero at egestas. Aliquam eget interdum enim. Nulla malesuada dolor at turpis blandit sagittis. </p>
                                   <h4 className="client-name">John Doe</h4>
                                   <p className="theme-color">Passanger</p>
                               </div>
                               </OwlCarousel>;
                       </div>
                   </div>
               </div>
           </section>
           <section className="section-padding blog-section">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6 offset-lg-3">
                           <h2 className="section-title text-center">Our Blog</h2>
                       </div>
                   </div>
                   <div className="row">
                       <div className="col-lg-4 col-sm-6">
                           <div className="single-blog-item">
                               <div className="blog-img">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/blog/1.jpg`} alt="" />
                               </div>
                               <div className="blog-text">
                                   <h4 className="blog-heading">Pellentesque et finibus lacus, vitae
                                       malesuada tortor.</h4>
                                   <p className="blog-time">june 13,  2018</p>
                                   <a href="#" className="button button-dark tiny">Read More</a>
                               </div>
                           </div>
                       </div>
                       <div className="col-lg-4 col-sm-6">
                           <div className="single-blog-item">
                               <div className="blog-img">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/blog/2.jpg`}  alt="" />
                               </div>
                               <div className="blog-text">
                                   <h4 className="blog-heading">Pellentesque et finibus lacus, vitae
                                       malesuada tortor.</h4>
                                   <p className="blog-time">june 13,  2018</p>
                                   <a href="#" className="button button-dark tiny">Read More</a>
                               </div>
                           </div>
                       </div>
                       <div className="col-lg-4 offset-lg-0 col-sm-6 offset-sm-3">
                           <div className="single-blog-item">
                               <div className="blog-img">
                                   <img src={`${process.env.PUBLIC_URL}/assets/images/blog/3.jpg`}  alt="" />
                               </div>
                               <div className="blog-text">
                                   <h4 className="blog-heading">Pellentesque et finibus lacus, vitae
                                       malesuada tortor.</h4>
                                   <p className="blog-time">june 13,  2018</p>
                                   <a href="#" className="button button-dark tiny">Read More</a>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       </div>
    );
  }
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(HomeComponets);
