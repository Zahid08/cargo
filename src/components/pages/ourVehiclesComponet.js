import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link } from "react-router-dom";


class OurVehiclesComponet extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }
  render() {
      console.log(`${process.env.REACT_APP_API_URL}`);
    return (
       <div>
           <section className="breadcrumb-section">
               <div className="container">
                   <div className="row">
                       <div className="col-6">
                           <ol className="breadcrumb">
                               <li><a href="#">Ride with Cabgo</a></li>
                           </ol>
                       </div>
                       <div className="col-6">
                           <div className="text-right">
                               <h2>Our vehicles</h2>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <section className="section-padding our-vehicles-section">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6 offset-lg-3">
                           <h2 className="section-title text-center">Our vehicles</h2>
                       </div>
                   </div>
                   <div className="row">
                       <div className="col-lg-12 text-center">
                           <div className="vehicle-tab-nav">
                               <ul className="nav nav-tabs" role="tablist">
                                   <li role="presentation" className="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab" className="active">All</a></li>
                                   <li role="presentation"><a href="#scooty" aria-controls="scooty" role="tab" data-toggle="tab">Scooter</a></li>
                                   <li role="presentation"><a href="#economy" aria-controls="economy" role="tab" data-toggle="tab">Economy</a></li>
                                   <li role="presentation"><a href="#luxury" aria-controls="luxury" role="tab" data-toggle="tab">Luxury</a></li>
                                   <li role="presentation"><a href="#suv" aria-controls="suv" role="tab" data-toggle="tab">SUV</a></li>
                                   <li role="presentation"><a href="#limusine" aria-controls="limusine" role="tab" data-toggle="tab">Limusine</a></li>
                               </ul>
                           </div>
                           <div className="tab-content vehicle-tab-content">
                               <div role="tabpanel" className="tab-pane active" id="all">
                                   <div className="row">
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/21_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/22_our_vehicles.png`}  alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/23_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/21_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/22_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/23_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`}  alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/21_our_vehicles.png`}  alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/22_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/23_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/21_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/22_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/23_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <div role="tabpanel" className="tab-pane" id="scooty">
                                   <div className="row">
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`}  alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/21_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/22_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/23_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`}  alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/21_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/22_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/23_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <div role="tabpanel" className="tab-pane" id="economy">
                                   <div className="row">
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/21_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/22_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/23_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/21_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/22_our_vehicles.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <div role="tabpanel" className="tab-pane" id="luxury">
                                   <div className="row">
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`}  alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img  src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`}  alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <div role="tabpanel" className="tab-pane" id="suv">
                                   <div className="row">
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} nalt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <div role="tabpanel" className="tab-pane" id="limusine">
                                   <div className="row">
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`}  alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                       <div className="col-lg-3 col-md-6">
                                           <div className="single-vehicle-box">
                                               <div className="single-vehilce-img">
                                                   <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`}  alt="vehicle img" />
                                               </div>
                                               <div className="vehicle-box-hover">
                                                   <h4>Car Name here</h4>
                                                   <p>LXI Optional998 cc, Manual, Petrol, 24.07 kmpl</p>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div className="row">
                       <div className="col-lg-12">
                           <nav aria-label="page navigation" className="page-navigation">
                               <ul className="pagination">
                                   <li className="page-item"><a className="page-link" href="#">Previous</a></li>
                                   <li className="page-item"><a className="page-link active" href="#">1</a></li>
                                   <li className="page-item"><a className="page-link" href="#">2</a></li>
                                   <li className="page-item"><a className="page-link" href="#">3</a></li>
                                   <li className="page-item"><a className="page-link" href="#">4</a></li>
                                   <li className="page-item"><a className="page-link" href="#">...</a></li>
                                   <li className="page-item"><a className="page-link" href="#">115</a></li>
                                   <li className="page-item"><a className="page-link" href="#">Next</a></li>
                               </ul>
                           </nav>
                       </div>
                   </div>
               </div>
           </section>
       </div>
    );
  }
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(OurVehiclesComponet);
