import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import HeaderTwo from "../common/headersTwo";
import FootersOne from "../common/footersOne";


class SignUpComponet extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }
  render() {
      const css = `
            body.theme-1 {
            background-image: url(../../assets/images/striped-bg.jpg);
            }`;

      return (
          <div>
              <HeaderTwo/>
              <div>
                  <style>{css}</style>
                  <section className="section-padding p-t-0 signup-section user-access-bg">
                      <div className="container">
                          <div className="row">
                              <div className="col-lg-6 offset-lg-3 text-center">
                                  <h2>Sign in</h2>
                              </div>
                          </div>
                          <div className="row">
                              <div className="col-lg-6 offset-lg-3">
                                  <div className="account-access sign-up">
                                      <ul className="nav nav-tabs" role="tablist">
                                          <li role="presentation" className="active">
                                              <a href="#rider" className="active" aria-controls="rider" role="tab" data-toggle="tab">Sign up to Ride</a>
                                          </li>
                                          <li role="presentation">
                                              <a href="#driver" aria-controls="driver" role="tab" data-toggle="tab">Sign up to Drive</a>
                                          </li>
                                      </ul>
                                      <div className="tab-content">
                                          <div role="tabpanel" className="tab-pane active" id="rider">
                                              <form className="user-access-form">
                                                  <div className="row">
                                                      <div className="col-lg-6">
                                                          <div className="form-group">
                                                              <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="First Name" />
                                                          </div>
                                                      </div>
                                                      <div className="col-lg-6">
                                                          <div className="form-group">
                                                              <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Last Name" />
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div className="form-group">
                                                      <input type="phone" className="form-control" id="exampleInputphone" placeholder="Phone number" />
                                                  </div>
                                                  <div className="form-group">
                                                      <input type="email" className="form-control" id="exampleInputPassword1" placeholder="Email" />
                                                  </div>
                                                  <div className="form-group">
                                                      <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                                                  </div>
                                                  <button type="submit" className="button button-dark btn-block">Sign in Now</button>
                                              </form>
                                              <p className="acclink">Already have an account?
                                                  <Link to={`${process.env.PUBLIC_URL}/sign-in`}>Sign in<i className="icofont">double_right</i></Link>
                                              </p>
                                          </div>
                                          <div role="tabpanel" className="tab-pane" id="driver">
                                              <form className="user-access-form">
                                                  <div className="row">
                                                      <div className="col-lg-6">
                                                          <div className="form-group">
                                                              <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="First Name" />
                                                          </div>
                                                      </div>
                                                      <div className="col-lg-6">
                                                          <div className="form-group">
                                                              <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Last Name" />
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div className="form-group">
                                                      <input type="phone" className="form-control" id="exampleInputphone" placeholder="Phone number" />
                                                  </div>
                                                  <div className="form-group">
                                                      <input type="email" className="form-control" id="exampleInputPassword1" placeholder="Email" />
                                                  </div>
                                                  <div className="form-group">
                                                      <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Create Password" />
                                                  </div>
                                                  <div className="form-group">
                                                      <input type="text" className="form-control" id="exampleInputPassword1" placeholder="City" />
                                                  </div>
                                                  <div className="form-group">
                                                      <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Invite Code" />
                                                  </div>
                                                  <button type="submit" className="button button-dark btn-block">Sign in Now</button>
                                              </form>
                                              <p className="acclink">Already have an account?
                                                  <Link to={`${process.env.PUBLIC_URL}/sign-in`}>Sign in<i className="icofont">double_right</i></Link>
                                              </p>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </section>
              </div>
              <FootersOne/>
          </div>
    );
  }
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(SignUpComponet);
