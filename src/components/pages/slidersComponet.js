import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import OwlCarousel from 'react-owl-carousel';


class SlidersComponet extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }
  render() {
    return (
       <div>
           <section className="hero-area">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-12">
                               <OwlCarousel className="hero-area-slider owl-carousel" loop={true} nav={false} autoWidth={false} items={1} center={false} id="hero-area-slider">
                                   <div className="single-slider-item">
                                       <div className="hero-area-left">
                                           <h1>Earn. Connect. Contribute to Socity</h1>
                                           <p>Partner with us to drive your own livelihood and more.</p>
                                           <Link to={`${process.env.PUBLIC_URL}/signup`}  className="button button-dark big">Sign up No</Link>
                                       </div>
                                       <div className="hero-area-right">
                                           <img src={`${process.env.PUBLIC_URL}/assets/images/home/1.png`} alt="" />
                                       </div>
                                   </div>
                                   <div className="single-slider-item">
                                       <div className="hero-area-left">
                                           <h1>Earn. Connect. Contribute to Socity</h1>
                                           <p>Partner with us to drive your own livelihood and more.</p>
                                           <Link to={`${process.env.PUBLIC_URL}/signup`}  className="button button-dark big">Sign up Now</Link>
                                       </div>
                                       <div className="hero-area-right">
                                           <img src={`${process.env.PUBLIC_URL}/assets/images/home/1.png`} alt="" />
                                       </div>
                                   </div>
                                   <div className="single-slider-item">
                                       <div className="hero-area-left">
                                           <h1>Earn. Connect. Contribute to Socity</h1>
                                           <p>Partner with us to drive your own livelihood and more.</p>
                                           <Link to={`${process.env.PUBLIC_URL}/signup`}  className="button button-dark big">Sign up Now</Link>
                                       </div>
                                       <div className="hero-area-right">
                                           <img src={`${process.env.PUBLIC_URL}/assets/images/home/1.png`}  alt="" />
                                       </div>
                                   </div>
                           </OwlCarousel>;
                       </div>
                   </div>
               </div>
           </section>
       </div>
    );
  }
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(SlidersComponet);
