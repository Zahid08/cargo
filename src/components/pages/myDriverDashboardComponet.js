import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import FootersOne from "../common/footersOne";
import HeaderOne from "../common/headersOne";


class MyDriverDashboardComponet extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }
  render() {
      console.log(`${process.env.REACT_APP_API_URL}`);
    return (
        <div>
            <HeaderOne/>
           <div>
               <section className="breadcrumb-section">
                   <div className="container">
                       <div className="row">
                           <div className="col-6">
                               <ol className="breadcrumb">
                                   <li><a href={`${process.env.PUBLIC_URL}/`}>Home</a></li>
                                   <li><a href={`${process.env.PUBLIC_URL}/my-driver-dashboard`}>My dashboard</a></li>
                               </ol>
                           </div>
                           <div className="col-6">
                               <div className="text-right">
                                   <h2>Contact us</h2>
                               </div>
                           </div>
                       </div>
                   </div>
               </section>
               <section className="section-padding driver-dashboard-section">
                   <div className="container">
                       <div className="row">
                           <div className="col-sm-6">
                               <div className="passanger-name">
                                   <div className="media">
                                       <img className="mr-3" src={`${process.env.PUBLIC_URL}/assets/images/partner-img.png`} alt="partner-img" />
                                       <div className="media-body">
                                           <h2 className="mt-0">Johnson Smith</h2>
                                           <p>ID 1234567890</p>
                                           <a href="#">Edit Profile</a>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <div className="col-sm-6 right-text">
                               <h2>Partnership with CarrGo</h2>
                           </div>
                       </div>
                       <div className="row">
                           <div className="col-lg-12">
                               <div className="tab-dashboard">
                                   <ul className="nav nav-tabs tab-navigation" role="tablist">
                                       <li role="presentation" className="active">
                                           <a href="#dashboard" aria-controls="dashboard" className="active" role="tab" data-toggle="tab">Dashboard</a>
                                       </li>
                                       <li role="presentation" className="active">
                                           <a href="#info" aria-controls="info" role="tab" data-toggle="tab">Personal Information</a>
                                       </li>
                                       <li role="presentation">
                                           <a href="#message" aria-controls="message" role="tab" data-toggle="tab">Message</a>
                                       </li>
                                       <li role="presentation">
                                           <a href="#vehicles" aria-controls="vehicles" role="tab" data-toggle="tab">Vehicles</a>
                                       </li>
                                       <li role="presentation">
                                           <a href="#drivers" aria-controls="drivers" role="tab" data-toggle="tab">Drivers</a>
                                       </li>
                                       <li role="presentation">
                                           <a href="#rides" aria-controls="rides" role="tab" data-toggle="tab">Rides</a>
                                       </li>
                                       <li role="presentation">
                                           <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a>
                                       </li>
                                   </ul>
                                   <div className="tab-content">
                                       <div role="tabpanel" className="tab-pane active" id="dashboard">
                                           <div className="dashboard-info">
                                               <div className="ride-chart small-section">
                                                   <h4>Ride From the day</h4>
                                                   <div className="small-section-item">
                                                       <div id="ride-chart" />
                                                   </div>
                                               </div>
                                               <div className="overview-counter small-section">
                                                   <h4>Overview</h4>
                                                   <div className="counter-wrapper bg-gray small-section-item">
                                                       <div className="single-counter-box">
                                                           <h2 className="counter-number">18445</h2>
                                                           <p className="counter-text">Total Rides</p>
                                                       </div>
                                                       <div className="single-counter-box">
                                                           <h2 className="counter-number">21785</h2>
                                                           <p className="counter-text">Total Passengers</p>
                                                       </div>
                                                       <div className="single-counter-box">
                                                           <h2 className="counter-number">150</h2>
                                                           <p className="counter-text">Drivers</p>
                                                       </div>
                                                       <div className="single-counter-box">
                                                           <h2 className="counter-number">75</h2>
                                                           <p className="counter-text">Today Rides</p>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div className="earning-details small-section">
                                                   <h4>Total earnings of last month</h4>
                                                   <div className="total-earning-table table-responsive small-section-item">
                                                       <table className="table">
                                                           <thead>
                                                           <tr>
                                                               <th scope="col">Name of Cabs</th>
                                                               <th scope="col">Earnigns</th>
                                                               <th scope="col">Date</th>
                                                               <th scope="col">Drivers</th>
                                                           </tr>
                                                           </thead>
                                                           <tbody>
                                                           <tr>
                                                               <th scope="row">BMW 5 <small>“4976ART RU”</small></th>
                                                               <td>$337.29</td>
                                                               <td>May 11, 2018</td>
                                                               <td>Johnson Smith</td>
                                                           </tr>
                                                           <tr>
                                                               <th scope="row">Audi <small>“4876ORT AU”</small></th>
                                                               <td>$856.56</td>
                                                               <td>May 11, 2018</td>
                                                               <td>John Doe</td>
                                                           </tr>
                                                           <tr>
                                                               <th scope="row">Alto XL <small>“4865ART KU”</small></th>
                                                               <td>$186.00</td>
                                                               <td>May 11, 2018</td>
                                                               <td>Rock William</td>
                                                           </tr>
                                                           <tr>
                                                               <th scope="row">Swift Dezire <small>“9856BRU PO”</small></th>
                                                               <td>$847.25</td>
                                                               <td>May 11, 2018</td>
                                                               <td>Jassica</td>
                                                           </tr>
                                                           <tr>
                                                               <th scope="row">BMW 5 <small>“4976ART RU”</small></th>
                                                               <td>$1337.29</td>
                                                               <td>May 11, 2018</td>
                                                               <td>Elly Smith</td>
                                                           </tr>
                                                           <tr>
                                                               <th scope="row">Tesia  <small>“68946KUY UK”</small></th>
                                                               <td>$869.29</td>
                                                               <td>May 11, 2018</td>
                                                               <td>Stone Gold</td>
                                                           </tr>
                                                           <tr>
                                                               <th scope="row">Audi 8 <small>“4976ART RU”</small></th>
                                                               <td>$537.29</td>
                                                               <td>May 11, 2018</td>
                                                               <td>Rock</td>
                                                           </tr>
                                                           <tr>
                                                               <th scope="row">Honda City XL <small>“8766ART TU”</small></th>
                                                               <td>$225.50</td>
                                                               <td>May 11, 2018</td>
                                                               <td>Johnson Doe</td>
                                                           </tr>
                                                           <tr>
                                                               <th scope="row">Alto XL <small>“3589PMT MB”</small></th>
                                                               <td>$100.00</td>
                                                               <td>May 11, 2018</td>
                                                               <td>John William</td>
                                                           </tr>
                                                           </tbody>
                                                       </table>
                                                   </div>
                                               </div>
                                               <div className="text-center">
                                                   <a href="#" className="button button-dark tiny">View More</a>
                                               </div>
                                           </div>
                                       </div>
                                       <div role="tabpanel" className="tab-pane" id="info">
                                           <div className="personal-info small-section">
                                               <div className="section-heading">
                                                   <h4 className="heading-item heading-item-1">Personal Information</h4>
                                                   <p className="heading-item heading-item-2 right">
                                                       <a href="#" className="edit-btn"><i className="fas fa-edit" /> Edit</a>
                                                   </p>
                                               </div>
                                               <div className="personal-details small-section-item">
                                                   <div className="row">
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>First Name</label>
                                                               <input type="text" className="form-control text-muted" readOnly defaultValue="John" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Last Name</label>
                                                               <input type="text" className="form-control text-muted" readOnly defaultValue="Doe" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Your Email</label>
                                                               <input type="text" className="form-control text-muted" readOnly defaultValue="johndoe@gmail.com" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Your Website</label>
                                                               <input type="text" className="form-control text-muted" readOnly defaultValue="www.johndoe.com" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Your Birthday</label>
                                                               <input type="text" className="form-control text-muted" readOnly defaultValue="01 June 1984" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Your Phone Number</label>
                                                               <input type="text" className="form-control text-muted" readOnly defaultValue="+91 - 123 456 7890" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Your Gender</label>
                                                               <input type="text" className="form-control text-muted" readOnly defaultValue="Male" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Status</label>
                                                               <input type="text" className="form-control text-muted" readOnly defaultValue="Married" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-12">
                                                           <div className="form-group">
                                                               <label htmlFor>Write a little description about you</label>
                                                               <textarea className="form-control text-muted" readOnly defaultValue={"Vestibulum suscipit faucibus dolor, vitae mollis justo consequat vel. Vestibulum in nisi ut neque tristique accumsan vel eu eros. Quisque pellentesque urna et hendrerit lacinia. Mauris vitae tellus neque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec in placerat tortor, sit amet dictum sem. Donec et orci condimentum eros pulvinar maximus. Suspendisse accumsan imperdiet mauris vitae tincidunt. Donec imperdiet purus eget diam tristique vestibulum. Vestibulum posuere placerat lacus commodo sollicitudin. Nullam eget justo fermentum, rhoncus leo eget, viverra augue. Fusce odio odio, egestas id turpis at, faucibus consectetur nulla. Sed vel volutpat ligula, quis vulputate odio. Sed condimentum, neque nec aliquam sodales, dolor erat euismod erat, porta venenatis odio leo non dolor. Donec ut lacus non quam convallis sodales."} />
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div role="tabpanel" className="tab-pane" id="message">
                                           <h4>Messages</h4>
                                           <div className="message-box small-section">
                                               <div className="row">
                                                   <div className="col-xl-4">
                                                       <div className="event-msg-left">
                                                           <ul className="list-group">
                                                               <li className="list-group-item msg-single ">
                                                                   <div className="sidebar-heading">
                                                                       <h4>Messages</h4>
                                                                   </div>
                                                                   <div className="event-sideber-search">
                                                                       <form action="#" method="post" className="search-form">
                                                                           <input type="text" className="form-control" placeholder="Search" />
                                                                           <i className="fa fa-search" />
                                                                       </form>
                                                                   </div>
                                                               </li>
                                                               <li className="list-group-item msg-single">
                                                                   <div className="media">
                                                                       <img className="mr-3 img-fluid" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`} alt="placeholder image" />
                                                                       <div className="media-body">
                                                                           <h5 className="mt-0">John</h5>
                                                                           <p className="mb-0">Cras sed sodales enim...</p>
                                                                           <p>
                                                                               <small>4 Hour ago</small>
                                                                           </p>
                                                                       </div>
                                                                       <div className="dropdown custom-dropdown">
                                                                           <div data-toggle="dropdown">
                                                                               <i className="fa fa-ellipsis-v msg-btn" />
                                                                           </div>
                                                                           <div className="dropdown-menu dropdown-menu-right">
                                                                               <a className="dropdown-item" href="#">Option 1</a>
                                                                               <a className="dropdown-item" href="#">Option 2</a>
                                                                               <a className="dropdown-item" href="#">Option 3</a>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </li>
                                                               <li className="list-group-item msg-single">
                                                                   <div className="media">
                                                                       <img className="mr-3 img-fluid" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`} alt="placeholder image" />
                                                                       <div className="media-body">
                                                                           <h5 className="mt-0">Rock</h5>
                                                                           <p className="mb-0">Cras sed sodales enim...</p>
                                                                           <p>
                                                                               <small>4 Hour ago</small>
                                                                           </p>
                                                                       </div>
                                                                       <div className="dropdown custom-dropdown">
                                                                           <div data-toggle="dropdown">
                                                                               <i className="fa fa-ellipsis-v msg-btn" />
                                                                           </div>
                                                                           <div className="dropdown-menu dropdown-menu-right">
                                                                               <a className="dropdown-item" href="#">Option 1</a>
                                                                               <a className="dropdown-item" href="#">Option 2</a>
                                                                               <a className="dropdown-item" href="#">Option 3</a>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </li>
                                                               <li className="list-group-item msg-single">
                                                                   <div className="media">
                                                                       <img className="mr-3 img-fluid" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`} alt="placeholder image" />
                                                                       <div className="media-body">
                                                                           <h5 className="mt-0">Johnson</h5>
                                                                           <p className="mb-0">Cras sed sodales enim...</p>
                                                                           <p>
                                                                               <small>4 Hour ago</small>
                                                                           </p>
                                                                       </div>
                                                                       <div className="dropdown custom-dropdown">
                                                                           <div data-toggle="dropdown">
                                                                               <i className="fa fa-ellipsis-v msg-btn" />
                                                                           </div>
                                                                           <div className="dropdown-menu dropdown-menu-right">
                                                                               <a className="dropdown-item" href="#">Option 1</a>
                                                                               <a className="dropdown-item" href="#">Option 2</a>
                                                                               <a className="dropdown-item" href="#">Option 3</a>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </li>
                                                               <li className="list-group-item msg-single">
                                                                   <div className="media">
                                                                       <img className="mr-3 img-fluid" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`}  alt="placeholder image" />
                                                                       <div className="media-body">
                                                                           <h5 className="mt-0">Smith</h5>
                                                                           <p className="mb-0">Cras sed sodales enim...</p>
                                                                           <p>
                                                                               <small>4 Hour ago</small>
                                                                           </p>
                                                                       </div>
                                                                       <div className="dropdown custom-dropdown">
                                                                           <div data-toggle="dropdown">
                                                                               <i className="fa fa-ellipsis-v msg-btn" />
                                                                           </div>
                                                                           <div className="dropdown-menu dropdown-menu-right">
                                                                               <a className="dropdown-item" href="#">Option 1</a>
                                                                               <a className="dropdown-item" href="#">Option 2</a>
                                                                               <a className="dropdown-item" href="#">Option 3</a>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </li>
                                                               <li className="list-group-item msg-single">
                                                                   <div className="media">
                                                                       <img className="mr-3 img-fluid" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`} alt="placeholder image" />
                                                                       <div className="media-body">
                                                                           <h5 className="mt-0">Akash</h5>
                                                                           <p className="mb-0">Cras sed sodales enim...</p>
                                                                           <p>
                                                                               <small>4 Hour ago</small>
                                                                           </p>
                                                                       </div>
                                                                       <div className="dropdown custom-dropdown">
                                                                           <div data-toggle="dropdown">
                                                                               <i className="fa fa-ellipsis-v msg-btn" />
                                                                           </div>
                                                                           <div className="dropdown-menu dropdown-menu-right">
                                                                               <a className="dropdown-item" href="#">Option 1</a>
                                                                               <a className="dropdown-item" href="#">Option 2</a>
                                                                               <a className="dropdown-item" href="#">Option 3</a>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </li>
                                                               <li className="list-group-item msg-single">
                                                                   <div className="media">
                                                                       <img className="mr-3 img-fluid" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`} alt="placeholder image" />
                                                                       <div className="media-body">
                                                                           <h5 className="mt-0">Akash</h5>
                                                                           <p className="mb-0">Cras sed sodales enim...</p>
                                                                           <p>
                                                                               <small>4 Hour ago</small>
                                                                           </p>
                                                                       </div>
                                                                       <div className="dropdown custom-dropdown">
                                                                           <div data-toggle="dropdown">
                                                                               <i className="fa fa-ellipsis-v msg-btn" />
                                                                           </div>
                                                                           <div className="dropdown-menu dropdown-menu-right">
                                                                               <a className="dropdown-item" href="#">Option 1</a>
                                                                               <a className="dropdown-item" href="#">Option 2</a>
                                                                               <a className="dropdown-item" href="#">Option 3</a>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </li>
                                                           </ul>
                                                       </div>
                                                   </div>
                                                   <div className="col-xl-8">
                                                       <div className="event-chat-ryt">
                                                           <ul className="list-group">
                                                               <li className="list-group-item">
                                                                   <div className="media">
                                                                       <div className="media-img">
                                                                           <img className="mr-3 img-fluid" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`} alt="placeholder image" />
                                                                       </div>
                                                                       <div className="media-body">
                                                                           <h3 className="mb-0">John</h3>
                                                                           <p>Online</p>
                                                                       </div>
                                                                       <div className="phone-icon">
                                                                           <a href="#"><i className="fas fa-phone" /></a>
                                                                       </div>
                                                                       <div className="dropdown custom-dropdown">
                                                                           <div data-toggle="dropdown">
                                                                               <i className="fa fa-ellipsis-v msg-btn" />
                                                                           </div>
                                                                           <div className="dropdown-menu dropdown-menu-right">
                                                                               <a className="dropdown-item" href="#">Option 1</a>
                                                                               <a className="dropdown-item" href="#">Option 2</a>
                                                                               <a className="dropdown-item" href="#">Option 3</a>
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                               </li>
                                                               <li className="list-group-item">
                                                                   <div className="char-area">
                                                                       <div className="chat-reciver">
                                                                           <div className="media">
                                                                               <div className="media-body">
                                                                                   <p>Lorem ipsum dolor sit amet.</p>
                                                                               </div>
                                                                               <img className="ml-3" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`} alt="user" />
                                                                           </div>
                                                                       </div>
                                                                       <div className="chat-sender">
                                                                           <div className="media">
                                                                               <img className="ml-3" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`}  alt="user" />
                                                                               <div className="media-body">
                                                                                   <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, saepe.</p>
                                                                               </div>
                                                                           </div>
                                                                       </div>
                                                                       <div className="chat-reciver">
                                                                           <div className="media">
                                                                               <div className="media-body">
                                                                                   <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos, aperiam.</p>
                                                                               </div>
                                                                               <img className="ml-3" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`} alt="user" />
                                                                           </div>
                                                                       </div>
                                                                       <div className="chat-sender">
                                                                           <div className="media">
                                                                               <img className="ml-3" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`} alt="user" />
                                                                               <div className="media-body">
                                                                                   <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Praesentium, sequi aliquid saepe hic alias optio?</p>
                                                                               </div>
                                                                           </div>
                                                                       </div>
                                                                       <div className="chat-reciver">
                                                                           <div className="media">
                                                                               <div className="media-body">
                                                                                   <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos, aperiam.</p>
                                                                               </div>
                                                                               <img className="ml-3" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`} alt="user" />
                                                                           </div>
                                                                       </div>
                                                                       <div className="chat-sender">
                                                                           <div className="media">
                                                                               <img className="ml-3" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`} alt="user" />
                                                                               <div className="media-body">
                                                                                   <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Praesentium, sequi aliquid saepe hic alias optio?</p>
                                                                               </div>
                                                                           </div>
                                                                       </div>
                                                                       <div className="chat-reciver">
                                                                           <div className="media">
                                                                               <div className="media-body">
                                                                                   <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos, aperiam.</p>
                                                                               </div>
                                                                               <img className="ml-3" src={`${process.env.PUBLIC_URL}/assets/images/messenger/1.png`} alt="user" />
                                                                           </div>
                                                                       </div>
                                                                   </div>
                                                                   <div className="char-type">
                                                                       <form className="d-flex justify-content-center" action="#" method="post">
                                                                           <input type="text" className="form-control" placeholder="Type Here..." />
                                                                           <button className="button button-dark">SEND</button>
                                                                       </form>
                                                                   </div>
                                                               </li>
                                                           </ul>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div role="tabpanel" className="tab-pane" id="vehicles">
                                           <div className="vahicles-container">
                                               <div className="row">
                                                   <div className="col-lg-6">
                                                       <h4>My vehicles</h4>
                                                   </div>
                                                   <div className="col-lg-6 text-right">
                                                       <a href="#" className="button button-dark">Register New Vehicle</a>
                                                   </div>
                                               </div>
                                               <div className="row">
                                                   <div className="col-lg-3 col-sm-6">
                                                       <div className="single-vehicle-container">
                                                           <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="Vehicle" />
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-sm-6">
                                                       <div className="single-vehicle-container">
                                                           <img src={`${process.env.PUBLIC_URL}/assets/images/21_our_vehicles.png`} alt="Vehicle" />
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-sm-6">
                                                       <div className="single-vehicle-container">
                                                           <img src={`${process.env.PUBLIC_URL}/assets/images/22_our_vehicles.png`} alt="Vehicle" />
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-sm-6">
                                                       <div className="single-vehicle-container">
                                                           <img src={`${process.env.PUBLIC_URL}/assets/images/23_our_vehicles.png`} alt="Vehicle" />
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-sm-6">
                                                       <div className="single-vehicle-container">
                                                           <img src={`${process.env.PUBLIC_URL}/assets/images/dashboard/vehicle-1.png`} alt="Vehicle" />
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-sm-6">
                                                       <div className="single-vehicle-container">
                                                           <img src={`${process.env.PUBLIC_URL}/assets/images/21_our_vehicles.png`} alt="Vehicle" />
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-sm-6">
                                                       <div className="single-vehicle-container">
                                                           <img src={`${process.env.PUBLIC_URL}/assets/images/22_our_vehicles.png`} alt="Vehicle" />
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-sm-6">
                                                       <div className="single-vehicle-container">
                                                           <img src={`${process.env.PUBLIC_URL}/assets/images/23_our_vehicles.png`} alt="Vehicle" />
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div role="tabpanel" className="tab-pane" id="drivers">
                                           <div className="drivers-info">
                                               <h4>Drivers</h4>
                                               <div className="row small-section">
                                                   <div className="col-lg-3 col-md-6">
                                                       <div className="single-driver">
                                                           <div className="card">
                                                               <img className="card-img-top" src={`${process.env.PUBLIC_URL}/assets/images/dashboard/driver-1.png`} alt="Card image cap" />
                                                               <div className="card-body">
                                                                   <h4 className="card-title">John Smith</h4>
                                                                   <p className="card-text">(+1) 123 456 7890</p>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-md-6">
                                                       <div className="single-driver">
                                                           <div className="card">
                                                               <img className="card-img-top"  src={`${process.env.PUBLIC_URL}/assets/images/14_my_driver_dashboard_my_drivers.png`} alt="Card image cap" />
                                                               <div className="card-body">
                                                                   <h4 className="card-title">John Smith</h4>
                                                                   <p className="card-text">(+1) 123 456 7890</p>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-md-6">
                                                       <div className="single-driver">
                                                           <div className="card">
                                                               <img className="card-img-top" src={`${process.env.PUBLIC_URL}/assets/images/15.png`} alt="Card image cap" />
                                                               <div className="card-body">
                                                                   <h4 className="card-title">John Smith</h4>
                                                                   <p className="card-text">(+1) 123 456 7890</p>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-md-6">
                                                       <div className="single-driver">
                                                           <div className="card">
                                                               <img className="card-img-top" src={`${process.env.PUBLIC_URL}/assets/images/16.png`} alt="Card image cap" />
                                                               <div className="card-body">
                                                                   <h4 className="card-title">John Smith</h4>
                                                                   <p className="card-text">(+1) 123 456 7890</p>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-md-6">
                                                       <div className="single-driver">
                                                           <div className="card">
                                                               <img className="card-img-top" src={`${process.env.PUBLIC_URL}/assets/images/15.png`} alt="Card image cap" />
                                                               <div className="card-body">
                                                                   <h4 className="card-title">John Smith</h4>
                                                                   <p className="card-text">(+1) 123 456 7890</p>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-md-6">
                                                       <div className="single-driver">
                                                           <div className="card">
                                                               <img className="card-img-top" src={`${process.env.PUBLIC_URL}/assets/images/19.png`} alt="Card image cap" />
                                                               <div className="card-body">
                                                                   <h4 className="card-title">John Smith</h4>
                                                                   <p className="card-text">(+1) 123 456 7890</p>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-md-6">
                                                       <div className="single-driver">
                                                           <div className="card">
                                                               <img className="card-img-top" src={`${process.env.PUBLIC_URL}/assets/images/20.png`} alt="Card image cap" />
                                                               <div className="card-body">
                                                                   <h4 className="card-title">John Smith</h4>
                                                                   <p className="card-text">(+1) 123 456 7890</p>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                                   <div className="col-lg-3 col-md-6">
                                                       <div className="single-driver">
                                                           <div className="card">
                                                               <img className="card-img-top" src={`${process.env.PUBLIC_URL}/assets/images/21.png`} alt="Card image cap" />
                                                               <div className="card-body">
                                                                   <h4 className="card-title">John Smith</h4>
                                                                   <p className="card-text">(+1) 123 456 7890</p>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div role="tabpanel" className="tab-pane" id="rides">
                                           <div className="rides-details">
                                               <div className="row">
                                                   <div className="col-lg-6">
                                                       <h4>Rides</h4>
                                                   </div>
                                                   <div className="col-lg-6">
                                                       <div className="rides-filter">
                                                           <ul>
                                                               <li>
                                                                   <a href="#">Yesterday</a>
                                                               </li>
                                                               <li>
                                                                   <a href="#">Last Week</a>
                                                               </li>
                                                               <li>
                                                                   <a href="#">Last Month</a>
                                                               </li>
                                                               <li>
                                                                   <a href="#">Last 6 Month</a>
                                                               </li>
                                                               <li>
                                                                   <a href="#">Last Year</a>
                                                               </li>
                                                           </ul>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div className="row small-section">
                                                   <div className="col-lg-12">
                                                       <div className="total-earning-table table-responsive">
                                                           <table className="table">
                                                               <thead>
                                                               <tr>
                                                                   <th scope="col">Name of Cabs</th>
                                                                   <th scope="col">Earnigns</th>
                                                                   <th scope="col">Date</th>
                                                                   <th scope="col">Passengers</th>
                                                               </tr>
                                                               </thead>
                                                               <tbody>
                                                               <tr>
                                                                   <th scope="row">BMW 5 <small>“4976ART RU”</small></th>
                                                                   <td>$337.29</td>
                                                                   <td>May 11, 2018</td>
                                                                   <td>Johnson Smith</td>
                                                               </tr>
                                                               <tr>
                                                                   <th scope="row">Audi <small>“4876ORT AU”</small></th>
                                                                   <td>$856.56</td>
                                                                   <td>May 11, 2018</td>
                                                                   <td>John Doe</td>
                                                               </tr>
                                                               <tr>
                                                                   <th scope="row">Alto XL <small>“4865ART KU”</small></th>
                                                                   <td>$186.00</td>
                                                                   <td>May 11, 2018</td>
                                                                   <td>Rock William</td>
                                                               </tr>
                                                               <tr>
                                                                   <th scope="row">Swift Dezire <small>“9856BRU PO”</small></th>
                                                                   <td>$847.25</td>
                                                                   <td>May 11, 2018</td>
                                                                   <td>Jassica</td>
                                                               </tr>
                                                               <tr>
                                                                   <th scope="row">BMW 5 <small>“4976ART RU”</small></th>
                                                                   <td>$1337.29</td>
                                                                   <td>May 11, 2018</td>
                                                                   <td>Elly Smith</td>
                                                               </tr>
                                                               <tr>
                                                                   <th scope="row">Tesia  <small>“68946KUY UK”</small></th>
                                                                   <td>$869.29</td>
                                                                   <td>May 11, 2018</td>
                                                                   <td>Stone Gold</td>
                                                               </tr>
                                                               <tr>
                                                                   <th scope="row">Audi 8 <small>“4976ART RU”</small></th>
                                                                   <td>$537.29</td>
                                                                   <td>May 11, 2018</td>
                                                                   <td>Rock</td>
                                                               </tr>
                                                               <tr>
                                                                   <th scope="row">Honda City XL <small>“8766ART TU”</small></th>
                                                                   <td>$225.50</td>
                                                                   <td>May 11, 2018</td>
                                                                   <td>Johnson Doe</td>
                                                               </tr>
                                                               <tr>
                                                                   <th scope="row">Alto XL <small>“3589PMT MB”</small></th>
                                                                   <td>$100.00</td>
                                                                   <td>May 11, 2018</td>
                                                                   <td>John William</td>
                                                               </tr>
                                                               </tbody>
                                                           </table>
                                                       </div>
                                                       <div className="text-center">
                                                           <a href="#" className="button button-dark tiny">View More</a>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div role="tabpanel" className="tab-pane" id="settings">
                                           <div className="personal-info">
                                               <div className="row">
                                                   <div className="col-lg-6">
                                                       <h4>Personal Information</h4>
                                                   </div>
                                                   <div className="col-lg-6 text-right">
                                                       <a href="#"><i className="fas fa-edit" /> Edit</a>
                                                   </div>
                                               </div>
                                               <div className="personal-details">
                                                   <div className="row">
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>First Name</label>
                                                               <input type="text" className="form-control text-muted" defaultValue="John" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Last Name</label>
                                                               <input type="text" className="form-control text-muted" defaultValue="Doe" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Your Email</label>
                                                               <input type="text" className="form-control text-muted" defaultValue="johndoe@gmail.com" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Your Website</label>
                                                               <input type="text" className="form-control text-muted" defaultValue="www.johndoe.com" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Your Birthday</label>
                                                               <input type="text" className="form-control text-muted" defaultValue="01 June 1984" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Your Phone Number</label>
                                                               <input type="text" className="form-control text-muted" defaultValue="+91 - 123 456 7890" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Your Gender</label>
                                                               <input type="text" className="form-control text-muted" defaultValue="Male" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-6">
                                                           <div className="form-group">
                                                               <label htmlFor>Status</label>
                                                               <input type="text" className="form-control text-muted" defaultValue="Married" />
                                                           </div>
                                                       </div>
                                                       <div className="col-lg-12">
                                                           <div className="form-group">
                                                               <label htmlFor>Write a little description about you</label>
                                                               <textarea className="form-control text-muted" defaultValue={"Vestibulum suscipit faucibus dolor, vitae mollis justo consequat vel. Vestibulum in nisi ut neque tristique accumsan vel eu eros. Quisque pellentesque urna et hendrerit lacinia. Mauris vitae tellus neque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec in placerat tortor, sit amet dictum sem. Donec et orci condimentum eros pulvinar maximus. Suspendisse accumsan imperdiet mauris vitae tincidunt. Donec imperdiet purus eget diam tristique vestibulum. Vestibulum posuere placerat lacus commodo sollicitudin. Nullam eget justo fermentum, rhoncus leo eget, viverra augue. Fusce odio odio, egestas id turpis at, faucibus consectetur nulla. Sed vel volutpat ligula, quis vulputate odio. Sed condimentum, neque nec aliquam sodales, dolor erat euismod erat, porta venenatis odio leo non dolor. Donec ut lacus non quam convallis sodales."} />
                                                           </div>
                                                           <a href="#" className="button button-dark tiny">Save</a>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </section>
           </div>
            <FootersOne/>
        </div>
    );
  }
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(MyDriverDashboardComponet);
