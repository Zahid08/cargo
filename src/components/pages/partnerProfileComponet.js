import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import HeaderOne from "../common/headersOne";
import FootersOne from "../common/footersOne";


class PartnerProfileComponet extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }
  render() {
      console.log(`${process.env.REACT_APP_API_URL}`);
    return (
        <div>
            <HeaderOne/>
            <div>
                <section className="breadcrumb-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-6">
                                <ol className="breadcrumb">
                                    <Link to={`${process.env.PUBLIC_URL}/`}>Home</Link>
                                    <Link to={`${process.env.PUBLIC_URL}/partner-profile`}>Other partner profile</Link>
                                </ol>
                            </div>
                            <div className="col-6">
                                <div className="text-right">
                                    <h2>John Smith</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section-padding personal-information-section">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-6">
                                <div className="passanger-name">
                                    <div className="media">
                                        <img className="mr-3" src="../assets/images/partner-img.png" alt="partner-img" />
                                        <div className="media-body">
                                            <h2 className="mt-0">Johnson Smith</h2>
                                            <p>ID 1234567890</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6 text-right">
                                <a href="#" className="button button-dark tiny">Message</a>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="tab-dashboard">
                                    <ul className="nav nav-tabs tab-navigation" role="tablist">
                                        <li role="presentation" className="active">
                                            <a className="active" href="#info" aria-controls="info" role="tab" data-toggle="tab">Personal Information</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#vehicles" aria-controls="vehicles" role="tab" data-toggle="tab">Vehicles</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#drivers" aria-controls="drivers" role="tab" data-toggle="tab">Drivers</a>
                                        </li>
                                    </ul>
                                    {/* Tab panes */}
                                    <div className="tab-content">
                                        <div role="tabpanel" className="tab-pane active" id="info">
                                            <div className="personal-info">
                                                <h4>Personal Information</h4>
                                                <div className="personal-details">
                                                    <div className="row">
                                                        <div className="col-lg-6">
                                                            <div className="form-group">
                                                                <label htmlFor>First Name</label>
                                                                <input type="text" className="form-control text-muted" readOnly defaultValue="John" />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6">
                                                            <div className="form-group">
                                                                <label htmlFor>Last Name</label>
                                                                <input type="text" className="form-control text-muted" readOnly defaultValue="Doe" />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6">
                                                            <div className="form-group">
                                                                <label htmlFor>Your Email</label>
                                                                <input type="text" className="form-control text-muted" readOnly defaultValue="johndoe@gmail.com" />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6">
                                                            <div className="form-group">
                                                                <label htmlFor>Your Website</label>
                                                                <input type="text" className="form-control text-muted" readOnly defaultValue="www.johndoe.com" />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6">
                                                            <div className="form-group">
                                                                <label htmlFor>Your Birthday</label>
                                                                <input type="text" className="form-control text-muted" readOnly defaultValue="01 June 1984" />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6">
                                                            <div className="form-group">
                                                                <label htmlFor>Your Phone Number</label>
                                                                <input type="text" className="form-control text-muted" readOnly defaultValue="+91 - 123 456 7890" />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6">
                                                            <div className="form-group">
                                                                <label htmlFor>Your Gender</label>
                                                                <input type="text" className="form-control text-muted" readOnly defaultValue="Male" />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6">
                                                            <div className="form-group">
                                                                <label htmlFor>Status</label>
                                                                <input type="text" className="form-control text-muted" readOnly defaultValue="Married" />
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-12">
                                                            <div className="form-group">
                                                                <label htmlFor>Write a little description about you</label>
                                                                <textarea className="form-control text-muted" readOnly defaultValue={"Vestibulum suscipit faucibus dolor, vitae mollis justo consequat vel. Vestibulum in nisi ut neque tristique accumsan vel eu eros. Quisque pellentesque urna et hendrerit lacinia. Mauris vitae tellus neque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec in placerat tortor, sit amet dictum sem. Donec et orci condimentum eros pulvinar maximus. Suspendisse accumsan imperdiet mauris vitae tincidunt. Donec imperdiet purus eget diam tristique vestibulum. Vestibulum posuere placerat lacus commodo sollicitudin. Nullam eget justo fermentum, rhoncus leo eget, viverra augue. Fusce odio odio, egestas id turpis at, faucibus consectetur nulla. Sed vel volutpat ligula, quis vulputate odio. Sed condimentum, neque nec aliquam sodales, dolor erat euismod erat, porta venenatis odio leo non dolor. Donec ut lacus non quam convallis sodales."} />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" className="tab-pane" id="vehicles">
                                            <div className="vahicles-container">
                                                <div className="row">
                                                    <div className="col-lg-6">
                                                        <h4>My vehicles</h4>
                                                    </div>
                                                    <div className="col-lg-6 text-right">
                                                        <a href="#" className="button button-dark">Register New Vehicle</a>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-vehicle-container">
                                                            <img src="../assets/images/dashboard/vehicle-1.png" alt="Vehicle" />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-vehicle-container">
                                                            <img src="../assets/images/dashboard/vehicle-1.png" alt="Vehicle" />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-vehicle-container">
                                                            <img src="../assets/images/dashboard/vehicle-1.png" alt="Vehicle" />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-vehicle-container">
                                                            <img src="../assets/images/dashboard/vehicle-1.png" alt="Vehicle" />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-vehicle-container">
                                                            <img src="../assets/images/dashboard/vehicle-1.png" alt="Vehicle" />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-vehicle-container">
                                                            <img src="../assets/images/dashboard/vehicle-1.png" alt="Vehicle" />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-vehicle-container">
                                                            <img src="../assets/images/dashboard/vehicle-1.png" alt="Vehicle" />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-vehicle-container">
                                                            <img src="../assets/images/dashboard/vehicle-1.png" alt="Vehicle" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" className="tab-pane" id="drivers">
                                            <div className="drivers-info">
                                                <h4>Drivers</h4>
                                                <div className="row small-section">
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-driver">
                                                            <div className="card">
                                                                <img className="card-img-top" src="../assets/images/dashboard/driver-1.png" alt="Card image cap" />
                                                                <div className="card-body">
                                                                    <h4 className="card-title">John Smith</h4>
                                                                    <p className="card-text">(+1) 123 456 7890</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-driver">
                                                            <div className="card">
                                                                <img className="card-img-top" src="../assets/images/dashboard/driver-1.png" alt="Card image cap" />
                                                                <div className="card-body">
                                                                    <h4 className="card-title">John Smith</h4>
                                                                    <p className="card-text">(+1) 123 456 7890</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-driver">
                                                            <div className="card">
                                                                <img className="card-img-top" src="../assets/images/dashboard/driver-1.png" alt="Card image cap" />
                                                                <div className="card-body">
                                                                    <h4 className="card-title">John Smith</h4>
                                                                    <p className="card-text">(+1) 123 456 7890</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-driver">
                                                            <div className="card">
                                                                <img className="card-img-top" src="../assets/images/dashboard/driver-1.png" alt="Card image cap" />
                                                                <div className="card-body">
                                                                    <h4 className="card-title">John Smith</h4>
                                                                    <p className="card-text">(+1) 123 456 7890</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-driver">
                                                            <div className="card">
                                                                <img className="card-img-top" src="../assets/images/dashboard/driver-1.png" alt="Card image cap" />
                                                                <div className="card-body">
                                                                    <h4 className="card-title">John Smith</h4>
                                                                    <p className="card-text">(+1) 123 456 7890</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-driver">
                                                            <div className="card">
                                                                <img className="card-img-top" src="../assets/images/dashboard/driver-1.png" alt="Card image cap" />
                                                                <div className="card-body">
                                                                    <h4 className="card-title">John Smith</h4>
                                                                    <p className="card-text">(+1) 123 456 7890</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-driver">
                                                            <div className="card">
                                                                <img className="card-img-top" src="../assets/images/dashboard/driver-1.png" alt="Card image cap" />
                                                                <div className="card-body">
                                                                    <h4 className="card-title">John Smith</h4>
                                                                    <p className="card-text">(+1) 123 456 7890</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-6">
                                                        <div className="single-driver">
                                                            <div className="card">
                                                                <img className="card-img-top" src="../assets/images/dashboard/driver-1.png" alt="Card image cap" />
                                                                <div className="card-body">
                                                                    <h4 className="card-title">John Smith</h4>
                                                                    <p className="card-text">(+1) 123 456 7890</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <FootersOne/>
        </div>
    );
  }
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(PartnerProfileComponet);
