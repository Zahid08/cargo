import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Link } from "react-router-dom";


class ContactComponet extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }
  render() {
      console.log(`${process.env.REACT_APP_API_URL}`);
    return (
       <div>
           <section className="breadcrumb-section">
               <div className="container">
                   <div className="row">
                       <div className="col-6">
                           <ol className="breadcrumb">
                               <li><a href="#">Ride with Cabgo</a></li>
                           </ol>
                       </div>
                       <div className="col-6">
                           <div className="text-right">
                               <h2>Contact us</h2>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <section className="section-padding contact-info-section">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-4 col-sm-6">
                           <div className="single-contact-info text-center">
                               <img src={`${process.env.PUBLIC_URL}/assets/images/icon/contact_info.png`} alt="icon" />
                               <h4>Address</h4>
                               <p>Address : #1153, St No. 58, Hashtag Company Noida, Delhi, India.</p>
                           </div>
                       </div>
                       <div className="col-lg-4 col-sm-6">
                           <div className="single-contact-info text-center">
                               <img src={`${process.env.PUBLIC_URL}/assets/images/icon/contact_info-2.png`} alt="icon" />
                               <h4>Phone number</h4>
                               <p>Phone : +911234567890, +919581236897</p>
                           </div>
                       </div>
                       <div className="col-lg-4 offset-lg-0 col-sm-6 offset-sm-3">
                           <div className="single-contact-info text-center">
                               <img src={`${process.env.PUBLIC_URL}/assets/images/icon/contact_info-3.png`} alt="icon" />
                               <h4>E-mail</h4>
                               <p>Email : Gambol943@gmail.com</p>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
           <section className="section-padding contact-form-section p-t-0">
               <div className="container">
                   <div className="row">
                       <div className="col-lg-6">
                           <form action="#">
                               <h2>Send us an Email</h2>
                               <div className="row">
                                   <div className="col-lg-6">
                                       <input type="text" name id placeholder="Name" className="form-control" />
                                   </div>
                                   <div className="col-lg-6">
                                       <input type="text" name id placeholder="E-mail" className="form-control" />
                                   </div>
                                   <div className="col-lg-6">
                                       <input type="text" name id placeholder="Phone" className="form-control" />
                                   </div>
                                   <div className="col-lg-6">
                                       <input type="text" name id placeholder="Subject" className="form-control" />
                                   </div>
                                   <div className="col-lg-12">
                                       <textarea className="form-control" name id placeholder="Text Content" defaultValue={""} />
                                   </div>
                               </div>
                               <button className="button button-dark tiny" type="submit">Send</button>
                           </form>
                       </div>
                       <div className="col-lg-6">
                           <div className="contact-us-map">
                               <iframe frameBorder="0" style={{ width: "100%", height: "435px"}}
                                       src="https://www.google.com/maps/embed/v1/place?q=40.7127837,-74.0059413&amp;key=AIzaSyCc3zoz5TZaG3w2oF7IeR-fhxNXi8uywNk">
                               </iframe>
                           </div>
                       </div>
                   </div>
               </div>
           </section>
       </div>
    );
  }
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(ContactComponet);
