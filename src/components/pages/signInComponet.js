import React, { Component } from "react";
import { connect } from "react-redux";
import SlidersComponet from "./slidersComponet";
import HomeComponets from "./homeComponet";
import HeaderTwo from "../common/headersTwo";
import FootersOne from "../common/footersOne";
import {Link} from "react-router-dom";


class SignInComponet extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {

  }
  render() {

      const css = `
            body.theme-1 {
            background-image: url(../../assets/images/striped-bg.jpg);
            }`;

    return (
        <div>
            <HeaderTwo/>
            <div>
                <style>{css}</style>
                <section className="section-padding p-t-0 signin-section user-access-bg">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 offset-lg-3 text-center">
                                <h2>Sign in</h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-6 offset-lg-3">
                                <div className="account-access sign-in">
                                    <ul className="nav nav-tabs" role="tablist">
                                        <li role="presentation" className="active">
                                            <a href="#rider" className="active" aria-controls="rider" role="tab" data-toggle="tab">Rider</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#driver" aria-controls="driver" role="tab" data-toggle="tab">Driver</a>
                                        </li>
                                    </ul>
                                    <div className="tab-content">
                                        <div role="tabpanel" className="tab-pane active" id="rider">
                                            <form className="user-access-form">
                                                <div className="form-group">
                                                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email or Phone number" />
                                                </div>
                                                <div className="form-group">
                                                    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                                                </div>
                                                <button type="submit" className="button button-dark btn-block">Sign in Now</button>
                                            </form>
                                            <p className="acclink">Don't have an account?
                                                <Link to={`${process.env.PUBLIC_URL}/sign-up`}><i className="icofont">double_right</i>Sign Up</Link>
                                            </p>
                                            <div className="externel-signup">
                                                <a href="#" className="btn-block facebook">
                                                    <i className="fab fa-facebook-f" />
                                                    Sign up with Facebook</a>
                                                <a href="#" className="btn-block google">
                                                    <i className="fab fa-google" />
                                                    Sign up with Google</a>
                                            </div>
                                        </div>
                                        <div role="tabpanel" className="tab-pane" id="driver">
                                            <form className="user-access-form">
                                                <div className="form-group">
                                                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email or Phone number" />
                                                </div>
                                                <div className="form-group">
                                                    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                                                </div>
                                                <button type="submit" className="button button-dark btn-block">Sign in Now</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <FootersOne/>
        </div>
    );
  }
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps)(SignInComponet);
