import React, {Component} from 'react';
import { withTranslate } from 'react-redux-multilingual'

// Custom Components
//Import Header
import Headers from './common/headers';
//Import Footer
import Footers from "./common/footers";

class Layout extends Component {

    render() {
        return (
            <div>
                <Headers/>
                {this.props.children}
                <Footers/>
            </div>
        );
    }
}

export default withTranslate(Layout);
